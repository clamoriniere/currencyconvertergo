package rates

import (
	"currencyconverter/bom"
	"currencyconverter/database"
	"net/http"

	"github.com/emicklei/go-restful"
)

// Rates rates service
type Rates struct {
	db *database.Database
}

// NewRatesService returns new instance of a Rates
func NewRatesService(db *database.Database) *Rates {
	return &Rates{db: db}
}

// Register register the Rates service
func (r *Rates) Register(c *restful.Container) {
	ws := new(restful.WebService)
	ws.
		Path("/rates").
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON)

	ws.Route(ws.POST("/add").To(r.addRates))
	ws.Route(ws.GET("/list").To(r.listRates))
	ws.Route(ws.GET("/search").To(r.searchRates)).
		Param(ws.QueryParameter("from", "currency from").DataType("string")).
		Param(ws.QueryParameter("to", "currency to").DataType("string"))

	c.Add(ws)
}

func (r *Rates) addRates(req *restful.Request, resp *restful.Response) {
	c := &bom.Currency{}
	if err := req.ReadEntity(&c); err != nil {
		encodeError(resp, err)
	}

	if err := r.db.StoreCurrency(c); err != nil {
		encodeError(resp, err)
	}

	resp.WriteEntity(c)
}

func (r *Rates) listRates(req *restful.Request, resp *restful.Response) {
	currencies, err := r.db.ListCurrencies()
	if err == nil {
		resp.WriteEntity(currencies)
	} else {
		resp.WriteError(http.StatusInternalServerError, err)
	}
}

func (r *Rates) searchRates(req *restful.Request, resp *restful.Response) {
	c := &bom.Currency{
		From: req.QueryParameter("from"),
		To:   req.QueryParameter("to"),
	}
	var err error
	c, err = r.db.SearchCurrencies(c)
	if err == nil {
		resp.WriteEntity(c)
	} else {
		resp.WriteError(http.StatusInternalServerError, err)
	}
}

func encodeError(resp *restful.Response, err error) {
	resp.WriteErrorString(http.StatusBadRequest, err.Error())
}
