package money

import (
	"currencyconverter/bom"
	"currencyconverter/database"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/emicklei/go-restful"
)

// Money service
type Money struct {
	db *database.Database
}

// NewMoneyService returns new instance of a Money
func NewMoneyService(db *database.Database) *Money {
	return &Money{db: db}
}

// Register register the Money service
func (m *Money) Register(c *restful.Container) {
	ws := new(restful.WebService)
	ws.
		Path("/money").
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON)

	ws.Route(ws.POST("/convert").To(m.convert))

	c.Add(ws)
}

func (m *Money) convert(req *restful.Request, resp *restful.Response) {
	query := &request{}
	if err := req.ReadEntity(&query); err != nil {
		encodeError(resp, err)
		return
	}

	request, err := decodeQuery(query.Query)
	if err != nil {
		encodeError(resp, err)
		return
	}

	answer, err := m.buildResponse(request)
	if err != nil {
		encodeError(resp, err)
		return
	}

	resp.WriteEntity(answer)
}

type request struct {
	Query string
	value float32
	from  string
	to    string
}

type response struct {
	Answer string
}

func encodeError(resp *restful.Response, err error) {
	resp.WriteHeaderAndEntity(http.StatusInternalServerError, response{Answer: "I' sorry Dave. I'm afraid. I can't do that"})
}

func decodeQuery(input string) (*request, error) {
	request := &request{}
	var err error

	vals := strings.Split(input, " ")
	if len(vals) != 4 {
		return nil, errors.New("Invalid input")
	}

	valueFrom, err := strconv.ParseFloat(vals[0], 32)
	if err != nil {
		return nil, err
	}
	request.value = float32(valueFrom)

	if len(vals[1]) != 3 || len(vals[3]) != 3 {
		return nil, errors.New("Currency Not valid")
	}

	request.from = vals[1]
	request.to = vals[3]

	if vals[2] != "en" {
		return nil, errors.New("Invalid query")
	}

	return request, nil
}

func (m *Money) buildResponse(req *request) (*response, error) {

	currency := &bom.Currency{
		From: req.from,
		To:   req.to,
	}

	rate, err := m.db.SearchCurrencies(currency)
	if err != nil {
		return nil, err
	}

	var resultat = req.value * rate.Rate
	return &response{
		Answer: fmt.Sprintf("%.2f%s = %.2f%s", req.value, req.from, resultat, req.to),
	}, nil
}
