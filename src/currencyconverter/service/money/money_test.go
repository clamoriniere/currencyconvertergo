package money

import "testing"

func TestDecodeQueryValidString(t *testing.T) {

	input := "1.20 EUR en USD"
	request, err := decodeQuery(input)
	if err != nil {
		t.Error("decodeQuery should not returns an error:", err)
	}
	if request.from != "EUR" {
		t.Error("from should be equal to EUR, current:", request.from)
	}
	if request.to != "USD" {
		t.Error("to should be equal to USD, current:", request.to)
	}
	if request.value != 1.2 {
		t.Error("value should be equal to 1.20, current:", request.value)
	}
}

func TestDecodeQueryWrongString(t *testing.T) {

	input := "1.20 EUR en DSDSDS"
	_, err := decodeQuery(input)
	if err == nil {
		t.Error("decodeQuery should returns an error:")
	}
}

func TestDecodeQueryWrongString2(t *testing.T) {
	input := "1.20EUR en USD"
	_, err := decodeQuery(input)
	if err == nil {
		t.Error("decodeQuery should returns an error:")
	}
}

func TestDecodeQueryWrongString3(t *testing.T) {
	input := "1.20 EUR en USD blabla"
	_, err := decodeQuery(input)
	if err == nil {
		t.Error("decodeQuery should returns an error:")
	}
}
