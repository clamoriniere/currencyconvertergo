package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/emicklei/go-restful"
	_ "github.com/mattn/go-sqlite3"

	"currencyconverter/database"
	"currencyconverter/service/money"
	"currencyconverter/service/rates"
)

var (
	port             int
	databasePathName string
)

const (
	defaultPort             = 8080
	defaultDatabasePathName = "./local.db"
)

func main() {
	flag.IntVar(&port, "p", defaultPort, "server port")
	flag.StringVar(&databasePathName, "db", defaultDatabasePathName, "sqlite db path name")

	flag.Parse()

	run()
}

func run() {
	fmt.Println("Lengow CurrencyConverter services")
	db := database.NewDatabase(databasePathName)
	if err := db.Init(); err != nil {
		panic(err)
	}
	defer db.Close()

	container := restful.NewContainer()
	rates := rates.NewRatesService(db)
	rates.Register(container)
	money := money.NewMoneyService(db)
	money.Register(container)

	server := &http.Server{Addr: getAddr(port), Handler: container}
	log.Fatal(server.ListenAndServe())
}

func getAddr(port int) string {
	return fmt.Sprintf(":%d", port)
}
