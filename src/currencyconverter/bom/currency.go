package bom

// Currency currency structure
type Currency struct {
	From string  `db:"from"`
	To   string  `db:"to"`
	Rate float32 `db:"rate"`
}

// CurrencyList list of Currency
type CurrencyList []Currency
