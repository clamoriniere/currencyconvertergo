package database

import (
	"currencyconverter/bom"

	"io/ioutil"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

func TestInsertRetrieveCurrencies(t *testing.T) {
	fileName, close := createTmpFile(t)
	db := NewDatabase(fileName)
	db.Init()

	defer close()
	defer db.Close()

	validateRetrieveList(t, db, 0)

	// Now insert element
	c := &bom.Currency{
		From: "EUR",
		To:   "USD",
		Rate: 2,
	}
	err := db.StoreCurrency(c)
	if err != nil {
		t.Error("StoreCurrency error: ", err)
	}

	// should contains 1 item
	validateRetrieveList(t, db, 1)

	c2 := &bom.Currency{
		From: "USD",
		To:   "EUR",
		Rate: 1.5,
	}
	err = db.StoreCurrency(c2)
	if err != nil {
		t.Error("StoreCurrency error: ", err)
	}
	// should contains 2 item
	validateRetrieveList(t, db, 2)

	c3 := &bom.Currency{
		From: "USD",
		To:   "EUR",
	}

	c3, err = db.SearchCurrencies(c3)
	if err != nil {
		t.Error("SearchCurrency err:", err)
	}

	if c3.Rate != 1.5 {
		t.Error("Wrong Rate value, should be 1.5, current:", c3.Rate)
	}
}

// createTmpFile create a temporary file
// return the tmp file name, and a remove method
func createTmpFile(t *testing.T) (string, func()) {
	tf, err := ioutil.TempFile("", "test")
	if err != nil {
		t.Fatalf("err %s", err)
	}
	tf.Close()

	return tf.Name(), func() { os.Remove(tf.Name()) }
}

func validateRetrieveList(t *testing.T, db *Database, s int) {
	list, err := db.ListCurrencies()
	if err != nil {
		t.Error("Error when retrieving empty list, err: ", err)
	}
	if len(list) != s {
		t.Error("List size should be 0, current size:", len(list))
	}
}
