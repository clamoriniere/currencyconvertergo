package database

import (
	"database/sql"
	"fmt"

	"gopkg.in/gorp.v1"

	"currencyconverter/bom"
)

const (
	currencyTableName = "currencies"
)

// Database database struct
type Database struct {
	dbName string
	db     *sql.DB
	dbMap  *gorp.DbMap
}

// NewDatabase returns nez Database instance
func NewDatabase(name string) *Database {
	return &Database{dbName: name}
}

// Init init database
func (d *Database) Init() error {
	var err error
	d.db, err = sql.Open("sqlite3", d.dbName)
	if err != nil {
		return err
	}

	d.dbMap = &gorp.DbMap{Db: d.db, Dialect: gorp.SqliteDialect{}}

	// add a table, setting the table name to 'currencies' and
	// specifying that the Id property is an auto incrementing PK
	d.dbMap.AddTableWithName(bom.Currency{}, currencyTableName).SetKeys(false, "From", "To")

	// create the table. in a production system you'd generally
	// use a migration tool, or create the tables via scripts
	err = d.dbMap.CreateTablesIfNotExists()

	return err
}

// Close used to close properly possible resources
func (d *Database) Close() {
	if d.db != nil {
		d.db.Close()
	}
}

// StoreCurrency use to store Currency object
func (d *Database) StoreCurrency(c *bom.Currency) error {
	return d.dbMap.Insert(c)
}

// ListCurrencies returns currencies list
func (d *Database) ListCurrencies() (bom.CurrencyList, error) {
	var currencies bom.CurrencyList
	var err error
	_, err = d.dbMap.Select(&currencies, string("select * from "+currencyTableName))
	return currencies, err
}

// SearchCurrencies returns the corresponding currency thanks to the params
func (d *Database) SearchCurrencies(c *bom.Currency) (*bom.Currency, error) {
	rq := fmt.Sprintf("SELECT * FROM %s WHERE \"from\"=\"%s\" AND \"to\"=\"%s\";", currencyTableName, c.From, c.To)
	err := d.dbMap.SelectOne(c, rq)

	return c, err
}
