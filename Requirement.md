# Lengow Test Technique

## Problématique

Le but de ce test est de recréer une mini implémentation de la fonctionnalité de conversions de monnaies de Google. En effet vous pouvez par exemple, taper directement dans la barre de recherche, l’expression suivante:

“10 dollars américains en euro”.

Le résultat de la conversion sera le premier résultat de recherche

Il n’y a pas de piège, nous voulons simplement mesurer votre niveau technique.
Vous pouvez utiliser le SGBD que vous souhaitez.


## Tâches

Chaque tâche doit être faite avec les bonnes pratiques de développement et de tests et en respectant très rigoureusement la PEP8.


1. Enregistrement des taux de change en base de données

Créer un projet Django et une app “CurrencyConverter”
Dans cette app créer une commande Django permettant d’enregistrer en base de données les taux de change par rapport à l’euro présents à cette adresse:http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml

2. Création d’un parseur/lexeur

Avec la bibliothèque PLY (http://www.dabeaz.com/ply/) ou tout autre lexer/parser de votre choix, créer un interpréteur qui puisse convertir des montants en se basant sur les taux que vous avez enregistré précédemment.
La syntaxe devra être la suivante:

Conversion de 10.32 euros en dollars:

10.32 EUR en USD
10.32 EUR = 11.30 USD

Conversion de 151.67 dollars australiens en francs suisses:

151.67 AUD en CHF
151.67 AUD = 108.06 CHF

NB: les montants calculés sont arrondis à deux chiffres après la virgule.


3. Création d’un webservice de consultation

Créer ensuite un webservice avec DjangoRestFramework ou tout autre framework de création de webservices REST de votre choix. Ce webservice devra avoir la spécification suivante:
Il sera appelable en JSON via la méthode POST et l’url /money/convert
Il prendra en entrée un seul paramètre sous forme de chaîne de caractères “query” qui sera l’expression rentrée par l’utilisateur.
Il retournera une unique valeur “answer” sous forme de texte contenant la réponse du parseur écrit précédemment
Conversion de 10.32 euros en dollars:

```
POST /money/convert
{
"query": "10.32 EUR en USD"
}
```

```
HTTP/1.1 200 OK
{
"answer": "10.32 EUR = 11.30 USD"
}
```


Conversion de 151.67 dollars australiens en francs suisses:

```
POST /money/convert
{
"query": "151.67 AUD en CHF"
}
```

```
HTTP/1.1 200 OK
{
"answer": "151.67 AUD = 108.06 CHF"
}
```

Recherche ne respectant le format d’entrée :

```
POST /money/convert
{
"query": "42"
}
```

```
HTTP/1.1 500 UnknownError
{
"answer": "I' sorry Dave. I'm afraid. I can't do that"
}
```