# Lengow Currency Rate service

## Services

### Add Currency Rate

* /rates/add (POST): add a CurrencyRate in the application db.

```
Request:
POST /rates/add
{
    "currency_from": "USD",
    "currency_to": "EUR",
    "rate":0.8
}

Response:
201
{
    "currency_from": "USD",
    "currency_to": "EUR",
    "rate":0.8
}
```

### List Currency Rates

* /rates (GET): returns available list of CurrencyRate


```
Request:
GET /rates

Response:
200
[
    { "currency_from": "USD", "currency_to": "EUR", "rate":0.8},
    { "currency_from": "EUR", "currency_to": "USD", "rate":1.2}
]
```

### Search Currency Rates

* /rates/search (GET): returns available list of CurrencyRate


```
Request:
GET /rates/search?from=USD&to=EUR

Response:
200
{ "currency_from": "USD", "currency_to": "EUR", "rate":0.8},
```

### Money Convert

* /money/convert (POST): from a currency conversion request sentence returns
      an answer sentence that represents the currency ratio


```
Request:
POST /rates
{
    "query":"100 EUR en USD"
}

Response:
200
{
    "answer":"100 EUR = 120 USD"
}
```

## How to install

First you need to setup the project. It will download the golang tools and and vendor the dependencies project

```
make init
```

To build the CurrencyConverter binary.

```
make build
```

Now you can run the unit-tests.

```
make test
```

you can list the others available make targets.

```
make target
```

## Run CurrencyConverter Services

Nothing special just run

```
make run
```

## Test it

In another terminal you can run the following curl command to test the services.

### Add currency rate service

```
clamoriniere:~ $ curl -H "Content-Type: application/json" -X POST -d '{ "from": "USD", "to": "EUR", "rate":0.5 }' http://127.0.0.1:8080/rates/add
{"currency_from":"USD","currency_to":"EUR","rate":0.8}
```

### List currency rates service

```
clamoriniere:~ $ curl -H "Content-Type: application/json" http://127.0.0.1:8080/rates/list
[{"currency_from":"USD","currency_to":"EUR","rate":0.8},{"from":"EUR","to":"USD","rate":1.2}]
```

### List currency rates service

```
clamoriniere:~ $ curl -H "Content-Type: application/json" http://127.0.0.1:8080/rates/search?from=EUR&to=USD
[{"currency_from":"USD","currency_to":"EUR","rate":0.8},{"from":"EUR","to":"USD","rate":1.2}]
```

### Money Convert

* Valid request:
    ```
    clamoriniere:~ $ curl -H "Content-Type: application/json" -X POST -d '{ "query": "10.32 EUR en USD" }' http://127.0.0.1:8080/money/convert
    {"answer":"10.32 EUR = 8.26 USD"}%
    ```

* Bad request:
    ```
    clamoriniere:~ $ curl -H "Content-Type: application/json" -X POST -d '{ "query": "10.32 EUR bad request" }' http://127.0.0.1:8080/money/convert
    {"answer":"I' sorry Dave. I'm afraid. I can't do that"}%
    ```
